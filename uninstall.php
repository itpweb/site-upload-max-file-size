<?php

/**
 * Fired when the plugin is uninstalled.
 *
 * When populating this file, consider the following flow
 * of control:
 *
 * - This method should be static
 * - Check if the $_REQUEST content actually is the plugin name
 * - Run an admin referrer check to make sure it goes through authentication
 * - Verify the output of $_GET makes sense
 * - Repeat with other user roles. Best directly by using the links/query string parameters.
 * - Repeat things for multisite. Once for a single site in the network, once sitewide.
 *
 * This file may be updated more in future version of the Boilerplate; however, this is the
 * general skeleton and outline for how the file should work.
 *
 * For more information, see the following discussion:
 * https://github.com/tommcfarlin/WordPress-Plugin-Boilerplate/pull/123#issuecomment-28541913
 *
 * @link       http://example.com
 * @since      1.0.1
 *
 * @package    site-upload-max-file-size
 */

/** If uninstall not called from WordPress, then exit. */
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

/**
 * Important: Check if the file is the one
 * that was registered during the uninstall hook.
 * 
 * @link 		https://github.com/franz-josef-kaiser/WordPress-Plugin-Boilerplate/commit/bf2f3db98b07a76c431259fb2f8e30e5fe42bb85#diff-b09d719c3258085fbaa9273b146e3b8fR127
 */
if ( plugin_basename( __FILE__ ) !== WP_UNINSTALL_PLUGIN ) {
	exit;
}

/** If user not allowed, then exit. */
if ( ! current_user_can( 'activate_plugins' ) ) {
	exit;
}

check_admin_referer( 'bulk-plugins' );

/**
 * Delete all options for this plugin
 * 
 * @since 		1.0.1
 */
$option_name = 'upload_max_file_size_mb';

if ( is_multisite() ) {
	delete_site_option( $option_name );
	
	$sites = wp_get_sites( array(
		'limit' => -1
	) ); // args?

	foreach ( $sites as $site ) {
		$site_id = $site['blog_id'];
		switch_to_blog( $site_id );
		delete_option( $option_name ) ;
	}

	restore_current_blog();
} else {
	delete_option( $option_name );
}