# Site Upload Max File Size WordPress Plugin

- Author: [@itpwebgit](https://github.com/itpwebgit)
- Author Website: [http://itechplus.org](http://itechplus.org)
- Contributor(s): [@akadusei](https://github.com/akadusei)
- License: [GNU General Public License v2.0 or later](http://www.gnu.org/licenses/gpl-2.0.txt)

### Description

Site Upload Max File Size is used to set the upload maximum file size in a WordPress multisite on a per site basis.

The plugin adds an option field under media settings of each site that allows network admins on a multisite network to set the upload maximum file size for that particular site.

When using this plugin on a multisite, be sure to *network activate* to prevent site admins from disabling it.