<?php

/**
 * This class provides option to set max upload
 * file size for each site on a multisite install
 *
 * @link       		http://example.com
 * @since      		1.0.0
 *
 * @package    		size-upload-max-file-size
 * @subpackage 		size-upload-max-file-size/includes
 */

/**
 * This class provides option to set max upload
 * file size for each site on a multisite install
 *
 * @package 		size-upload-max-file-size
 * @subpackage 		size-upload-max-file-size/includes
 * @author     		iTech Plus <info@itechplus.org>
 */
class Site_Upload_Max_File_Size {
	
	/**
	 * Name of our plugins options.
	 *
	 * @since    1.0.0
	 * @access   private
	 *
	 * @var      string    		$option_name    		ID of our plugin soptions.
	 */
	//private $option_name;

	/**
	 * Autoloads upon instantiation of this class
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		//$this->option_name = 'sumfs_options';

	}

	/**
	 * Filter the upload size limit.
	 *
	 * @since    	1.0.0
	 * @access  	public
	 * 
	 * @author 		Drew Jaynes
	 * @link 		https://profiles.wordpress.org/DrewAPicture
	 * 
	 * @see 		https://core.trac.wordpress.org/timeline?from=2014-07-13T18%3A21%3A04Z&precision=second
	 * @see 		https://developer.wordpress.org/reference/hooks/upload_size_limit/
	 *
	 * @var			string 		$size 		Upload size limit (in bytes).
	 *
	 * @return 		int (maybe) 	Filtered size limit.
	 */
	public function set_upload_max_file_size( $size ) {
	        
        if ( $new_size = $this->get_option( 'upload_max_file_size_mb' ) ) {
        	$size = absint( $new_size ) * 1024 * 1024;
        }

        return $size;

	}

	/**
	 * Display upload max size setting in 'Right Now' dashboard widget.
	 *
	 * @since    	1.0.0
	 * @access  	public
	 *
	 * @action 		activity_box_end
	 * @see 		https://developer.wordpress.org/reference/functions/wp_dashboard_right_now/#source-code
	 *
	 * @return 		string 		Upload max file size notice.
	 */
	public function right_now_end() {

		if ( $size = $this->get_option() ) {
			echo '<p>' . sprintf( __( 'Maximum upload file size is %s', 'size-upload-max-file-size' ), '<strong>' . number_format_i18n( $this->valid_size( $size ) ) . ' MB</strong>' ) . '</p>';
		} elseif ( $size = get_site_option( 'fileupload_maxk' ) ) {
			echo '<p>' . sprintf( __( 'Maximum upload file size is %s', 'size-upload-max-file-size' ), '<strong>' . number_format_i18n( $this->valid_size( $size / 1024 ) ) . ' MB</strong>' ) . '</p>';
		}

	}

	/**
	 * Initialize our options.
	 *
	 * @since    	1.0.0
	 * @access  	public
	 *
	 * @see 		http://wpengineer.com/2139/adding-settings-to-an-existing-page-using-the-settings-api/
	 */
	public function admin_init() {

		register_setting(
			'media',
			'upload_max_file_size_mb',
			'absint'
		);
		
		add_settings_field(
			'upload_max_file_size_mb',
			__( 'Maximum Upload File Size', 'size-upload-max-file-size' ),
			array( $this, 'upload_max_file_size_mb_callback' ),
			'media',
			'default'
		);

	}

	/**
	 * Display and fill the form field
	 *
	 * @since    	1.0.0
	 * @access  	public
	 *
	 * @see 		http://wpengineer.com/2139/adding-settings-to-an-existing-page-using-the-settings-api/
	 */
	public function upload_max_file_size_mb_callback() {
		
		$value = absint( $this->get_option( 'upload_max_file_size_mb' ) );

		if ( is_multisite() && ! current_user_can( 'manage_network' ) ) {
			echo sprintf( __( 'Maximum upload file size is <strong>%s MB</strong>', 'size-upload-max-file-size' ), number_format_i18n( $value ) );
			return $value;
		}

		echo '<input id="upload_max_file_size_mb" name="upload_max_file_size_mb" type="number" value="' . $value . '" />
		<label for="upload_max_file_size_mb">' . sprintf( __( ' <strong>MB</strong>. <em>Cannot be more than %s MB</em>. Enter <em>0</em> to disable this option.', 'size-upload-max-file-size' ), number_format_i18n( $this->ini_upload_max_filesize_mb() ) ) . '</label>';
	
	}

	/**
	 * Ensure our file size is not bigger than
	 * size set in PHP.INI
	 *
	 * @since    	1.0.0
	 * @access  	public
	 *
	 * @var 		integer 		Size to compare to PHP.INI size
	 *
	 * @return 		integer 		Valid file size
	 */
	private function valid_size( $size ) {

		$size = absint( $size );

		$ini_upload_max_filesize_mb = absint( $this->ini_upload_max_filesize_mb() );

		$valid_size = min( $ini_upload_max_filesize_mb, $size );

		return $valid_size;

	}

	/**
	 * Get the upload max size set in PHP.INI (in MB)
	 *
	 * @since    	1.0.0
	 * @access  	public
	 *
	 * @return 		integer 		Upload max file size as set in PHP.INI
	 */
	private function ini_upload_max_filesize_mb() {
	    
	    $ini_size = ini_get( 'upload_max_filesize' );

	    $size = absint( substr( $ini_size, 0, -1 ) );

	    $unit = strtoupper( substr( trim( $ini_size ), -1 ) );
	    
	    switch ( $unit ) {
	        
	        case 'K':
	            $mb_value = $size / 1024;
	            break;

	        case 'M':
	        	$mb_value = $size;
	        	break;

	        case 'G':
	            $mb_value = $size * 1024;
	            break;

	        case 'T':
	            $mb_value = $size * 1024 * 1024;
	            break;

	        default:
	            $mb_value = 0;
	            break;
	    
	    }

	    return $mb_value;
	
	}

	/**
	 * A little hack to make sure our previous options remain intact once updated
	 * if current user does not have the capability to edit option.
	 * 
	 * Add this as filter to the pre_update_option hook.
	 * 
	 * @since    	1.0.0
	 * @access  	public
	 */
	public function update_settings_value( $value, $option, $old_value ) {
		
		if ( $option == 'upload_max_file_size_mb' ) {
			
			$old_value = $this->valid_size( $old_value );
			$value = $this->valid_size( $value );

			if ( is_admin() && isset( $_POST['option_page'] ) && 'media' == $_POST['option_page'] ) { /** Make sure we're on the right page */
				
				if ( is_multisite() && ! current_user_can( 'manage_network' ) ) {
					$value = $old_value;
				}

			}

		}

		return $value;

	}

	/**
	 * Get option
	 *
	 * Use this to get this plugin's options
	 *
	 * @since    	1.0.0
	 * @access  	private
	 *
	 * @var 		string 		Option to get
	 *
	 * @return 		mixed 		Option value
	 */
	private function get_option( $option = '' ) {

		return absint( $this->valid_size( get_option( 'upload_max_file_size_mb' ) ) );

	}

}