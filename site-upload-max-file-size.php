<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * this starts the plugin. Plugin is based on WordPress Plugin Boilerplate 3.0.0
 *
 * @link              http://example.com
 * @since             1.0.0
 * @package           sumfs
 *
 * @wordpress-plugin
 * Plugin Name:       Site Upload Max File Size
 * Plugin URI:        http://itechplus.org
 * Description:       This plugin adds options to each site's media settings screen that allow setting a max upload size for that site. In multisites, only network admins can change this value.
 * Version:           1.2.0
 * Author:            iTech Plus
 * Author URI:        http://itechplus.org/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       site-upload-max-file-size
 * Domain Path:       /languages
 * 
 * This plugin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * any later version.
 * 
 * This plugin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this plugin. If not, see http://www.gnu.org/licenses/gpl-2.0.txt.
 */

/** If this file is called directly, abort. */
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The core plugin class that is used to define internationalization,
 * dashboard-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-site-upload-max-file-size.php';

/**
 * Define a function to run our plugin
 *
 * @since 		1.0.0
 */
function run_site_upload_max_file_size() {

	$sumfs = new Site_Upload_Max_File_Size();

	add_action( 'admin_init', array( $sumfs, 'admin_init' ) );
	add_filter( 'pre_update_option', array( $sumfs, 'update_settings_value' ), 10, 3 );
 	add_filter( 'upload_size_limit', array( $sumfs, 'set_upload_max_file_size' ), 20 );
 	add_action( 'activity_box_end', array( $sumfs, 'right_now_end' ), 99 );

}

/**
 * Actually run our plugin
 *
 * @since 		1.0.0
 */
run_site_upload_max_file_size();